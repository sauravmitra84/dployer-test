<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Droplet;
use GrahamCampbell\DigitalOcean\Facades\DigitalOcean;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_blog()
    {
        // DUMMY Dataset - Will come from HTTP POST input
        $blogName = 'Hello World';
        $blogDesc = 'Greeting to the world';
        $domain_name = 'dwhcloud.com';

        // GET IMAGE ID
        $image_id = 0;
        $images = DigitalOcean::image()->getAll();
        foreach($images as $image) {
            if($image->name==env('DO_IMAGE_NAME'))
                $image_id = $image->id;    
        }

        // GET DO SSHKEY TO ASSOCIATE
        $key_id = 0;
        $keys = DigitalOcean::key()->getAll();
        foreach($keys as $key) {
            if($key->name==env('DO_SSHKEY_NAME'))
                $key_id = $key->id;    
        }

        // CREATE DB FOR BLOG DROPLET
        $hostname = str_random(16);
        $dbpassword = str_random(16);
        DB::connection('blogdb')->statement('CREATE DATABASE ' . $hostname . '_v1 CHARACTER SET "utf8"');
        DB::connection('blogdb')->statement("CREATE USER " . $hostname . "_u1@'127.0.0.1' IDENTIFIED BY \"" . $dbpassword . "\"");
        DB::connection('blogdb')->statement("CREATE USER " . $hostname . "_u1@'localhost' IDENTIFIED BY \"" . $dbpassword . "\"");
        DB::connection('blogdb')->statement("CREATE USER " . $hostname . "_u1@'%' IDENTIFIED BY \"" . $dbpassword . "\"");
        DB::connection('blogdb')->statement("GRANT ALL PRIVILEGES ON " . $hostname . "_v1.* TO " . $hostname . "_u1@'127.0.0.1'");
        DB::connection('blogdb')->statement("GRANT ALL PRIVILEGES ON " . $hostname . "_v1.* TO " . $hostname . "_u1@'localhost'");
        DB::connection('blogdb')->statement("GRANT ALL PRIVILEGES ON " . $hostname . "_v1.* TO " . $hostname . "_u1@'%'");

        // CREATE DROPLET
        $droplet = DigitalOcean::droplet()->create( $hostname, 'sgp1', 's-1vcpu-1gb', $image_id,
            false, false, true, [$key_id],
            "#cloud-config
            runcmd:
              - [ sed, -i, -e, 's|^DB_HOST.*$|DB_HOST=" . env('BLOG_DB_HOST') . "|', /srv/shared/.env]
              - [ sed, -i, -e, 's|^DB_DATABASE.*$|DB_DATABASE=" . $hostname . "_v1|', /srv/shared/.env ]
              - [ sed, -i, -e, 's|^DB_USERNAME.*$|DB_USERNAME=". $hostname . "_u1|', /srv/shared/.env ]
              - [ sed, -i, -e, 's|^DB_PASSWORD.*$|DB_PASSWORD=". $dbpassword . "|', /srv/shared/.env ]
              - [ bash, /root/installBlog.sh ]",
            false, [], ['laravel-webapp']);
            
            //- [ sed, -i, -e, 's|^git clone .*$|git clone https://github.com/akash-mitra/dployer.git codebase|', /root/updateCode.sh ]
        
        // WAIT & GET IP ASSOCIATED WITH DROPLET
        sleep (2);
        
        $droplet_id = $droplet->id;
        $droplet = DigitalOcean::droplet()->getById($droplet_id);
        foreach($droplet->networks as $network) {
            if($network->type=='public')
                $public_ip = $network->ipAddress;
            if($network->type=='private')
                $private_ip = $network->ipAddress;
        }

        // CREATE DOMAIN & A RECORD
        $domain = DigitalOcean::domain()->create($domain_name, $public_ip);
        $domain_record = DigitalOcean::domainRecord()->create($domain_name, 'A', 'www', $public_ip);
        
        //DigitalOcean::droplet()->create($hostname, 'sgp1', '1gb', 44185658, false, false, true, ['e0:7a:9d:53:61:d0:67:31:23:4c:26:b7:52:0c:c4:26'], false);
        //$droplet = Droplet::provision($hostname);

        $user = User::where('id', '=', auth()->user()->id)->firstOrFail();

        // MARK ENTRY FOR EACH BLOGSITE
        Droplet::create([
            'subscription_id' => 111111,
            'user_id' => $user->id,
            'name' => $blogName,
            'hostname' => $hostname,
            'domain' => $domain_name,
            'desc' => $blogDesc,
            'do_id' => $droplet->id,
            'memory' => $droplet->memory,
            'vcpus' => $droplet->vcpus,
            'disk' => $droplet->disk,
            'region' => $droplet->region->slug,
            'public_ip' => $public_ip,
            'private_ip' => $private_ip,
            'backup_enabled' => $droplet->backupsEnabled,
            'db_host' => env('BLOG_DB_HOST'),
            'database' => $hostname . '_v1',
            'db_user' => $hostname . '_u1',
            'db_password' => $dbpassword,
        ]);

        return 'Success';
    }

    public function secure_blog()
    {
        $local_script = env('LOCAL_SCRIPT');
        $ssh_key = env('SSHKEY');

        // DUMMY Dataset - Will come from HTTP POST input
        $public_ip = '128.199.224.40';
        $domain_name = 'dwhcloud.com';
        $email = 'saurav.karate@gmail.com';
        
        return exec("bash $local_script $public_ip $ssh_key $domain_name $email");
    }
}