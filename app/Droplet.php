<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GrahamCampbell\DigitalOcean\Facades\DigitalOcean;

class Droplet extends Model
{
    protected $fillable = [
        'user_id', 'subscription_id', 'name', 'desc',
        'do_id', 'memory', 'vcpus', 'disk', 'hostname', 'domain',
        'region', 'public_ip', 'private_ip', 'backup_enabled',
        'db_host', 'database', 'db_user', 'db_password'
    ];

    public static function provision($hostname)
    {
        //TODO raise error if hostname is blank
        //TODO Location and image id should come from some config file.
        // 'names', 'region', 'size', 'image', 'backups', 'ipv6', 'private_networking', 'ssh_keys', 'monitoring', 'volumes', 'tags'
        // 'laravel-server-snapshot'
        return DigitalOcean::droplet()->create($hostname, 'sgp1', '1gb', 44185658, false, false, true, ['e0:7a:9d:53:61:d0:67:31:23:4c:26:b7:52:0c:c4:26'], false);
    }

    public function getServerInstance()
    {
        return DigitalOcean::droplet()->getById($this->do_id);
    }

    public function getIp()
    {
        if (!empty($this->public_ip)) {
            return $this->public_ip;
        }

        $ip = $this->getServerInstance()->networks[0]->ipAddress;

        if (!empty($ip)) {
            $this->public_ip = $ip;
            $this->save();
        }

        return $ip;
    }

    public function getStatus()
    {
        return $this->getServerInstance()->status;
    }

    public function setDomain($domainName)
    {
        $domain = DigitalOcean::domain();

        if (!empty($this->domain)) {
            $domain->delete($this->domain);
        }

        $domain->create($domainName, $this->getIp());

        $this->domain = $domainName;

        $this->save();
    }
}
