#!/bin/bash
REMOTEHOST="$1"
SSHKEY="$2"
BLOGSITE="$3"
EMAIL="$4"

ssh -p 2222 -i ${SSHKEY} -o "StrictHostKeyChecking no" root@${REMOTEHOST} "bash /root/secureBlog.sh ${BLOGSITE} ${EMAIL}"
if [ $? -ne 0 ]; then
    echo "Error"
    exit -1
else
    echo "Success"
fi
